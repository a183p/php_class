<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>自訂使用者錯誤</title>
    </head>
    <body>
        <?php
        
        //驗證密碼
        function validPwd($pwd){
            if(trim($pwd)=="")  //不可空字串
                trigger_error ("密碼不可為空字串",E_USER_NOTICE);
            if(strlen($pwd)<=4)//不可太短
                trigger_error ("密碼不可小於等於4個字",E_USER_WARNING);
             if(is_numeric($pwd))//不可全數字
                trigger_error ("密碼不可全部為數字",E_USER_WARNING);
        }
        
         //自訂使用者錯誤處理函數
       function errorHandler($type,$msg,$file,$line){
           switch ($type){
               case E_USER_ERROR:
                   echo "自訂使用者致命錯誤<br>";
                   break;
               case E_USER_WARNING:
                   echo "自訂使用者警告錯誤<br>";
                   break;
               case E_USER_NOTICE:
                   echo "自訂使用者注意錯誤<br>";
           }
           
         $errorTime=date('Y年m月d日 h:i:s a');
           $err= "$errorTime  錯誤檔案：$file, 第$line 行, 發生了 $msg <br>\n";
           echo $err;
           error_log($err,3,"userErrors.log");
           if($type==E_USER_ERROR){
               echo "程式終止<br>";
               die();
           }
       }//end
       
       set_error_handler('errorHandler');   //設定自訂錯誤函數之處理函數
       
       
       validPwd("");
       echo "<hr>";
       validpwd("ad3");
        echo "<hr>";
         validpwd("123");
        echo "<hr>";
        
         validpwd("9989999999");
        echo "<hr>";
        
         validpwd("123abc");
        echo "<hr>";
        ?>
    </body>
</html>
