<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>使用自訂Exception</title>
    </head>
    <body>
        <?php
        include_once 'MyException.php';
        function validPwd($pwd){
            if(trim($pwd)=="")  //不可空字串
                throw new MyException("密碼不可為空字串");
                           if(strlen($pwd)<=4)//不可太短
                throw new MyException("密碼不可小於等於4個字");
                          if(is_numeric($pwd))//不可全數字
                 throw new MyException("密碼不可全部為數字");
              }
              
              try {
                  validPwd("123456");   
              } catch (MyException $ex) {
                  $ex->getErrMsg();
              }
        ?>
    </body>
</html>
