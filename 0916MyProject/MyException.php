<?php


class MyException extends Exception{
    public function getErrMsg(){
        echo "日期：" . date('Y年m月d日 h:i:s a')."<br>";
        echo "檔名：" . $this->getFile()."<br>";
        echo "行數：" . $this->getLine()."<br>";
        echo "訊息：" . $this->getMessage()."<br>";
   }
}
