<?php
abstract class Sharp {
    private $X;
    private $Y;
    
    abstract function area();
    
    public function perimeter(){
        
    }

}

class Circle extends Sharp{
    private $R;
    public function __construct($X,$Y,$R) {
        $this->X=$X;
        $this->Y=$Y;
        $this->R=$R;
    }
    public function area(){
        return 3.1415 * pow($this->R,2);
    }
            
}
