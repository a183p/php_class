<?php
class Human {
    private $h;
    private $w;
    function speak($str){
        echo "我會說$str";
    }
    function doHouseWork(){
        echo "我會做家事";
    }
}

interface Weapons{
    function shoot();
}

interface Car{
     const MAX_CC=10000;
     function type();
}

class Transformers extends Human implements Weapons,Car{
    private $weapons;
    private $carType;
    
    public function __construct($weapons,$carType,$str) {
        $this->weapons=$weapons;
        $this->carType=$carType;
        $this->speak($str);
    }
            
    function shoot(){
        
    }
    
    function type(){
        
    }
}
