<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once 'Employee.php';
        $emp1 = new Employee();
        $emp2 = new Manager();
        $emp3 = new Engineer();
        echo $emp1;
        echo $emp2;
        echo $emp3;
        echo "<hr>";
        function iam(Employee $emp){
            return "我是$emp";
        }
        
        echo iam($emp1);
        echo iam($emp2);
        echo iam($emp3);
        echo "<hr>";
        
        if ($emp2 instanceof Employee)
            echo "yes<br>";
        else echo "no<br>";
        
        ?>
    </body>
</html>
