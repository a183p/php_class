<?php
class Animal {
    
    private $color;
    
    public function __construct() {
        echo "動物<br>";
    }
    public function eat(){
        echo "動物吃";
    }
    

}

class Bird extends Animal{

    private $fly;

    public function __construct() {
        parent::__construct();
        echo "鳥<br>";
    }
     public function eat(){
        echo "鳥吃";
    }
      public function __toString() {
        return "我是一隻鳥鳥<br>";
    }   //物件以文字方法輸出
}
class Ostrich extends Bird{
    
    public function isFly($fly){
        $this->fly=$fly;
        $str=($this->fly)?"我會飛":"我不會飛";
        $str=$fly?"我會飛<br>":"我不會飛<br>";
        
                              
        return $str;
    }
        
}

class Lion extends Animal{
    public function __construct() {
        echo "獅子<br>";
    }
}

class Fish extends Animal{
    
}

//////////////////////////////////////////////////////////////////////////////////
///menber
class Menber{
    private $name;
    private $pwd;
    private $status;
    
    final function isLogon(){
        return $this->status;
    }
    final function setStatus($status){
        $this->status=$status;
    }
}

final class BookMenber extends Menber{//final class 無法被繼承
    private $age;
    public function __construct($name,$pwd,$age) {
        $this->name=$name;
        $this->pwd=$pwd;
        $this->age=$age;
        $this->setStatus(false);
        
    }
    
}
//////////////////////////////////////////////////////////////////
