<?php

class Employee {
    public function __toString() {
        return "一個員工<br>";
    }
}

class Manager extends Employee{
    public function __toString() {
        return "一個經理<br>";
       
    }
}

class Engineer extends Employee{
    public function __toString() {
       return "一個工程師";
    }
}